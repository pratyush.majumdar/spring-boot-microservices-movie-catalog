# Springboot Microservice Movie Catalog
Microservice architecture is an architectural style that structures an application as a collection of services that are
1. Highly maintainable and testable
2. Loosely coupled
3. Independently deployable
4. Organized around business capabilities
5. Owned by a small team

The microservice architecture enables the rapid, frequent and reliable delivery of large, complex applications. It also enables an organization to evolve its technology stack.

## Microservice Patterns used in the project
1. The API Gateway pattern
2. Service discovery pattern
3. Decomposition patterns
4. Circuit Breaker pattern

### Applications [To be started in order]
1. Eureka Service Registry http://localhost:8761/
2. Config Server http://localhost:9296/actuator/info 
3. API Gateway http://localhost:8080/rest/catalog/
4. Hystrix Dashboard http://localhost:8181/hystrix
5. Microservices
	1. Catalog Microservice http://localhost:9001/rest/catalog/
6. External Zipkin Server http://localhost:9411/zipkin/
