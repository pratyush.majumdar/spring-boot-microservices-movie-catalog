package com.pratyush.catalog.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pratyush.catalog.entities.Catalog;
import com.pratyush.catalog.repositories.CatalogRepository;

@Service
public class CatalogService {
	@Autowired
	CatalogRepository catalogRepository;
	
	public List<Catalog> getAllCatalog() {
		List<Catalog> catalog = new ArrayList<>();
		Iterator<Catalog> iterator = catalogRepository.findAll().iterator();
		while(iterator.hasNext()) {
			catalog.add(iterator.next());
		}
		return catalog;
	}
	
	public Optional<Catalog> getCatalog(Long id) {
		return catalogRepository.findById(id);
	}
	
	public void addCatalog(Catalog catalog) {		
		catalogRepository.save(catalog);
	}
	
	public void updateCatalog(Long id, Catalog catalog) {		
		catalogRepository.save(catalog);
	}
	
	public void deleteCatalog(Catalog catalog) {
		catalogRepository.delete(catalog);
	}
}
