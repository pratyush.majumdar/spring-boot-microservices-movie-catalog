package com.pratyush.catalog.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pratyush.catalog.entities.Catalog;
import com.pratyush.catalog.service.CatalogService;

@RestController
public class CatalogController {
	@Autowired
	private CatalogService catalogService;
	
	@GetMapping(value = "/rest/catalog")
	public List<Catalog> getAllCatalog() {
		return catalogService.getAllCatalog();
	}
	
	@GetMapping(value="/rest/catalog/{id}")
	public Optional<Catalog> getCatalog(@PathVariable Long id) {
		return catalogService.getCatalog(id);
	}
	
	@PutMapping(value="/rest/catalog/{id}")
	public ResponseEntity<Catalog> updateCatalog(@RequestBody Catalog catalog, @PathVariable Long id) {
		try {
			catalogService.updateCatalog(id, catalog);
			return new ResponseEntity<Catalog>(catalog, HttpStatus.OK);
		}catch (NoSuchElementException e) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(value="/rest/catalog")
	public ResponseEntity<Catalog> addCatalog(@RequestBody Catalog catalog) {
		try {
			catalogService.addCatalog(catalog);
			return new ResponseEntity<Catalog>(catalog, HttpStatus.CREATED);
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
