package com.pratyush.catalog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pratyush.catalog.entities.Catalog;

@Repository
public interface CatalogRepository extends JpaRepository<Catalog, Long>{

}
