package com.pratyush.gateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallbackMethodController {
	
	@GetMapping("/catalogServiceFallBack")
	public String catalogServiceFallBack() {
		return "Service not Responding";
	}
}
